# Budgadier
An app to replace a budgeting spreadsheet.  Built with a React frontend connected via API to a .Net backend.

## To bootstrap the app
- download Docker
- create docker-compose symbolic link with `ln -sF docker/docker-compose.prod.yml docker-compose.yml`
- run `docker-compose build`
- symbolic link back to the dev compose file with `ln -sF docker/docker-compose.dev.yml docker-compose.yml`
- run `docker-compose up`
- Start developing!
