#!/bin/bash

echo '##  starting React entrypoint'
echo '-----------------------'

echo 'run startup script'

set -ex

# start appropriate server
if [ "$REACT_ENV" = "production" ]; then
  echo "Starting react production server"
  serve -s build
else
  echo "Starting react development server"
  yarn run start
fi
